import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet } from 'react-native';
import AppStack from './src/routes/AppStack';
import AppLoading from 'expo-app-loading';
import { useFonts, Roboto_400Regular, Roboto_500Medium } from '@expo-google-fonts/roboto';
import { Caladea_400Regular_Italic } from '@expo-google-fonts/caladea';

export default function App() {

  const [fontsLoaded] = useFonts({
    Roboto_400Regular,
    Roboto_500Medium,
    Caladea_400Regular_Italic,
  })

  if (!fontsLoaded) {
    return <AppLoading/>
  }

  return (
    <>
      <AppStack />
      <StatusBar 
        // barStyle="dark-content"
        backgroundColor="transparent"
        translucent
      />
    </>
  );
}

const styles = StyleSheet.create({
  
});
