export const StorageKeys = {
    TOKEN: 'token',
    USUARIO: 'usuario',
    LISTA_CELULAS: 'listaCelulas',
    CARGA_INICIAL: 'cargaInicial',
    CARGA_MEMBROS: 'cargaMembros',
    IGREJA: 'igreja',
}