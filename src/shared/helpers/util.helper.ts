import { Lider } from "../models/Celula.model";

export class UtilsHelper {

    // static ajustarFormatoData(dataString: string): string {
    //     string[] = dataString
    // }

    static diaSemana(numeroSemana: number): string {
        // let dayNamesShort = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'];
        let dayNamesShort = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
        return dayNamesShort[numeroSemana]
    }

    static nomesLideres(lideres: Lider[]) {
        let nomes: string = '';
        lideres.forEach(lider => {
            var nome: string[] = lider.nomeCompleto.split(" ");
            if (nomes == '') {
                nomes = `${nome[0]} `
            } else {
                nomes += `e ${nome[0]}`
            }
        })
        return nomes;
    }

    static dataAniversario(dataString: string): string {
        const dataNascimento = new Date(new Date(dataString).setDate(new Date(dataString).getDate() + 1));
        const monthNamesShort = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
        const dia = dataNascimento.getDate().toString().padStart(2, '0');
        const mes = monthNamesShort[dataNascimento.getMonth()];
        return `${dia}-${mes}`
    }

    static calcularIdade(dataNasc: string): number {
        var dataAtual = new Date();
        var anoAtual = dataAtual.getFullYear();
        var diaNasc = new Date(dataNasc).getDate();
        var mesNasc = new Date(dataNasc).getMonth();
        var anoNasc = new Date(dataNasc).getFullYear();
        var idade = anoAtual - anoNasc;
        var mesAtual = dataAtual.getMonth() + 1;
        if (mesAtual < mesNasc) {
            idade--;
        } else {
            if (mesAtual == mesNasc) {
                if (new Date().getDate() < diaNasc) {
                    idade--;
                }
            }
        }
        return idade;
    }

    static minEMaxData(dias: number): string {
        const data = new Date().setDate(new Date().getDate() + dias);
        return new Date(data).toISOString().split('T')[0];
    }

    static timeToString(time: number): string {
        const date: Date = new Date(time);
        const dateIso = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString();
        const data = new Date(dateIso)
        return data.toISOString().split('T')[0];
    }

    static getWeekDay(time: number): String {
        const date: Date = new Date(time);
        var diaSemana: String = '';
        switch (date.getDay()) {
            case 0:
                diaSemana = 'DOMINGO';
                break
            case 1:
                diaSemana = 'SEGUNDA';
                break
            case 2:
                diaSemana = 'TERCA';
                break
            case 3:
                diaSemana = 'QUARTA';
                break
            case 4:
                diaSemana = 'QUINTA';
                break
            case 5:
                diaSemana = 'SEXTA';
                break
            case 6:
                diaSemana = 'SABADO';
                break
        }
        return diaSemana;
    }

    static stgDiaSemana(dia: number): String {
        var diaSemana: String = '';
        switch (dia) {
            case 0:
                diaSemana = 'DOMINGO';
                break
            case 1:
                diaSemana = 'SEGUNDA';
                break
            case 2:
                diaSemana = 'TERCA';
                break
            case 3:
                diaSemana = 'QUARTA';
                break
            case 4:
                diaSemana = 'QUINTA';
                break
            case 5:
                diaSemana = 'SEXTA';
                break
            case 6:
                diaSemana = 'SABADO';
                break
        }
        return diaSemana;
    }

    static buscarEnumPerfilMembro(nome: string): string {
        var enumPerfil = '';
        switch (nome) {
            case 'ADULTO':
                enumPerfil = nome;
                break
            case 'CRIANÇA':
                enumPerfil = 'CRIANCA'
                break
        }
        return enumPerfil;
    }
}