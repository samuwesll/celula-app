import React, {useEffect} from 'react';
import { View, Text } from 'react-native';
import { EventoSemanal } from '../../models/Usuario.model';

import styles from './styles';

interface EventoProps {
    eventoSemanal?: EventoSemanal;
}

const Evento: React.FC<EventoProps> = ({ eventoSemanal }) => {

    useEffect(() => {
    },[])

    return (
        <View style={styles.container}>
            {eventoSemanal ?
                <View style={styles.evento}>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Evento: </Text>
                        {eventoSemanal.nome}
                    </Text>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Horário: </Text>
                        {eventoSemanal.horario}
                    </Text>
                    <Text style={styles.eventoText}>
                        <Text style={styles.eventoTextTitle}>Local: </Text>
                        {eventoSemanal.local}
                    </Text>
                </View>
                :
                null
            }
        </View>
    )
}

export default Evento;