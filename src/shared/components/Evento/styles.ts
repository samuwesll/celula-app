import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#d3d3d3',
        flex: 1,
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginTop: 17,
        justifyContent: 'space-between'
    },
    evento: {
        width: '75%',
    },
    eventoText: {
        margin: 2,
        color: '#fff'
    },
    eventoTextTitle: {
        fontWeight: 'bold',
        color: '#fff'
    },
});

export default styles;