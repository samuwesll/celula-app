import React, { cloneElement, useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { LocaleConfig, Agenda, Calendar } from 'react-native-calendars';

import { UtilsHelper } from '../../helpers/util.helper';
import { DiasCelula } from '../../models/CargaInicial.model';
import { Celula } from '../../models/Celula.model';
import { EventoProps } from '../../models/EventoProps.model';
import { CargaMembros } from '../../models/Membro.model';
import { EventoSemanal, Igreja } from '../../models/Usuario.model';
import Evento from '../Evento';
import EventoCelula from '../EventoCelula';

import styles from './styles'

interface CalendarioProps {
    corTheme: string,
    diasRelatorio: DiasCelula[],
    celulas: Celula[];
    cargaMembros: CargaMembros[],
    church: Igreja,
}
// 
const Calendario: React.FC<CalendarioProps> = ({ corTheme, diasRelatorio, celulas, cargaMembros, church }) => {

    const [marked, setMarked] = useState<Object>({});
    const [items, setItems] = useState<Object>({});

    LocaleConfig.locales['br'] = {
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan.', 'Fev.', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul.', 'Ago', 'Set.', 'Out.', 'Nov.', 'Dez.'],
        dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
        today: 'Hoje\'hui'
    };
    LocaleConfig.defaultLocale = 'br';

    async function loadItem(data: Date) {
        let objeto: any = new Object();
        let mark: any = new Object();
        const diasSemanaCelulas: string[] = celulas.map(cell => cell.dia)

        for (let i = -45; i < 7; i++) {
            const time = new Date().setDate(data.getDate() + i)
            const strTime = UtilsHelper.timeToString(time);
            const weekDay = UtilsHelper.getWeekDay(time);

            objeto[strTime] = [];
            if (diasSemanaCelulas.includes(weekDay as string)) {
                let evento: EventoProps = {
                    eventoCelula: true,
                    diasCelulas: [],
                    eventoSemana: [],
                }
                const dias = gerarEventoCelula(weekDay, strTime, celulas);
                if (dias.length >= 1) {
                    evento.diasCelulas = dias;
                    objeto[strTime] = [evento];
                    mark[strTime] = { marked: true, dotColor: 'green', selectedColor: 'green' }
                } else {
                    evento.diasCelulas = gerarEventoCelulaPendente(weekDay, strTime, celulas)
                    objeto[strTime] = [evento]
                    mark[strTime] = { marked: true, dotColor: 'red', selectedColor: 'red' };
                }
            }
            church.eventoSemanal?.forEach(sem => {
                let evento: EventoProps = {
                    eventoCelula: false,
                    diasCelulas: [],
                    eventoSemana: [],
                }
                sem.local = church.nome;
                if (weekDay == UtilsHelper.stgDiaSemana(sem.diaSemana) && i <= 14) {
                    evento.eventoSemana?.push(sem)
                    objeto[strTime] = [evento]
                    mark[strTime] = { marked: true, dotColor: '#d3d3d3', selectedColor: '#d3d3d3' }
                }
            })
        }

        setMarked(mark);
        setItems(objeto);
    }

    function gerarEventoCelula(diaSemana: String, data: string, celulas: Celula[]): DiasCelula[] {
        let retornoDias: DiasCelula[] = [];
        const idCelulas: number[] = celulas.filter(cell => cell.dia == diaSemana).map(c => c.id);
        retornoDias = diasRelatorio.filter(dia => dia.data == data && idCelulas.includes(dia.idCelula))
        return retornoDias;
    }

    function gerarEventoCelulaPendente(diaSemana: String, data: string, celulas: Celula[]): DiasCelula[] {
        let retornoDias: DiasCelula[] = [];
        celulas.filter(cell => cell.dia == diaSemana).forEach(cell => {
            retornoDias.push({
                data,
                idCelula: cell.id,
                nomeCelula: cell.nome,
                status: false,
            })
        })
        return retornoDias;
    }

    useEffect(() => {
        loadItem(new Date())
    }, [])

    return (
        <>
            <Agenda
                rowHasChanged={(r1: any, r2: any) => { return r1.text !== r2.text }}
                items={items as any}
                renderItem={((item: EventoProps, firstItemInDay) => {
                    return (
                        item.eventoCelula ?
                            <EventoCelula dias={item.diasCelulas as any} celulas={celulas} cargaMembros={cargaMembros} />
                            : <Evento eventoSemanal={item.eventoSemana[0] as any}/>
                    )
                })}
                renderEmptyDate={() => { return (<View />); }}
                minDate={UtilsHelper.minEMaxData(-30)}
                selected={new Date().toISOString().split('T')[0]}
                futureScrollRange={2}
                pastScrollRange={1}
                markedDates={marked as any}
                theme={{
                    agendaDayTextColor: `${corTheme}`,
                    agendaDayNumColor: `${corTheme}`,
                    // agendaTodayColor: 'black',
                    agendaKnobColor: `${corTheme}`
                }}
            />
        </>
    )
}

export default Calendario;