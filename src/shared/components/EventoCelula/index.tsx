import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, Modal, Alert, TextInput, ScrollView, ActivityIndicator } from 'react-native';
import api from '../../../shared/services/api';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Feather } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import styles from './styles';

import { CargaInicial, DiasCelula } from '../../models/CargaInicial.model';
import { Celula } from '../../models/Celula.model';
import { CargaMembros } from '../../models/Membro.model';
import { RelatorioCelulaRequest } from '../../models/Relatorio.model';
import { StorageKeys } from '../../const/storage-keys';
import { Response } from '../../models/Response.model';
import { Usuario } from '../../models/Usuario.model';
import ModalLoading from '../ModalLoading';
interface EventoCelulaProps {
    dias: DiasCelula[],
    celulas: Celula[],
    cargaMembros: CargaMembros[],
}

const EventoCelula: React.FC<EventoCelulaProps> = ({ dias, celulas, cargaMembros }) => {

    const [modalVisible, setModalVisible] = useState(false);
    const [idCelulaSelect, setIdCelulaSelect] = useState<number>()
    const [diaSelecionado, setDiaSelecionado] = useState<DiasCelula>();

    const [visitante, setVisitantes] = useState<string>('');
    const [oferta, setOferta] = useState<string>('');
    const [quiloAmor, setQuiloAmor] = useState<string>('');
    const [observacao, setObservacao] = useState<string>('');
    const [selectedItems, setSelectedItems] = useState<number[]>([]);
    const [loagind, setLoading] = useState<boolean>(false);

    function enviarRelatorio(evento: DiasCelula) {
        setIdCelulaSelect(celulas.filter(c => c.id == evento.idCelula)[0].id)
        setDiaSelecionado(evento)
    }

    async function editarRelatorio(evento: DiasCelula) {
        setIdCelulaSelect(celulas.filter(c => c.id == evento.idCelula)[0].id)
        setDiaSelecionado(evento)
        setVisitantes(evento.visitante as any)
        setQuiloAmor(Number.parseFloat(evento.quilos as any).toString())
        setOferta(Number.parseFloat(evento.ofertas as any).toString())
        setSelectedItems(evento.idMembros as any)
    }

    function filtrarCelula(idCelula: number): Celula {
        return celulas.filter(cell => cell.id == idCelula)[0]
    }

    function filtrarMembrosCelula(idCelula: number): CargaMembros {
        return cargaMembros.filter(c => c.idCelula == idCelula)[0];
    }

    function handleSelectItem(id: number) {
        const alreadySelected = selectedItems.findIndex(item => item == id)

        if (alreadySelected >= 0) {
            const filteredItems = selectedItems.filter(item => item !== id);
            setSelectedItems(filteredItems);
        } else {
            setSelectedItems([...selectedItems, id])
        }
    }

    async function handleRelatorioCelula() {
        let token = '';
        let idUsuario: number = 0;
        setLoading(true);

        await AsyncStorage.getItem(StorageKeys.TOKEN).then(r => {
            token = JSON.parse(r as any);
        }).catch(r => { })

        await AsyncStorage.getItem(StorageKeys.USUARIO).then(r => {
            let usuario: Usuario = JSON.parse(r as any);
            idUsuario = usuario.id;
        }).catch(r => { })

        const request: RelatorioCelulaRequest = {
            idLider: idUsuario,
            visitantes: visitante == '' ? 0 : Number.parseInt(visitante),
            kgAmor: quiloAmor == '' ? 0 : Number.parseFloat(quiloAmor.split(',').length > 0 ? quiloAmor.replace(',', '.') : quiloAmor),
            oferta: oferta == '' ? 0 : Number.parseFloat(oferta.split(',').length > 0 ? oferta.replace(',', '.') : oferta),
            idCelula: idCelulaSelect as any,
            dataRelatorio: diaSelecionado?.data as string,
            observaao: observacao,
            idsMembros: selectedItems,
        }

        await api.post<Response<CargaInicial>>('v1/relatorio', request, { headers: { 'Authorization': `Bearer ${token}` } }).then(response => {
            if (response.status == 201) {
                dias.forEach(dia => {
                    if (dia.data == request.dataRelatorio) {
                        dia.status = true;
                    }
                })
                AsyncStorage.removeItem(StorageKeys.CARGA_INICIAL).catch(e => { });
                AsyncStorage.setItem(StorageKeys.CARGA_INICIAL, JSON.stringify(response.data.sucesso.value));
            }
        }).catch(e => { })
        await setModalVisible(false)
        await setLoading(false);
    }

    async function handleEditarRelatorioCelula() {
        let token = '';
        let idUsuario: number = 0;
        setLoading(true);

        await AsyncStorage.getItem(StorageKeys.TOKEN).then(r => {
            token = JSON.parse(r as any);
        }).catch(r => { })

        await AsyncStorage.getItem(StorageKeys.USUARIO).then(r => {
            let usuario: Usuario = JSON.parse(r as any);
            idUsuario = usuario.id;
        }).catch(r => { })

        const request = {
            visitantes: visitante == '' ? 0 : Number.parseInt(visitante),
            kgAmor: quiloAmor == '' ? 0 : Number.parseFloat(quiloAmor.split(',').length > 0 ? quiloAmor.replace(',', '.') : quiloAmor),
            oferta: oferta == '' ? 0 : Number.parseFloat(oferta.split(',').length > 0 ? oferta.replace(',', '.') : oferta),
            observaao: observacao,
            idsMembros: selectedItems,
        }
        await api.put<Response<string>>(`v1/relatorio/${diaSelecionado?.id}`, request, { headers: { 'Authorization': `Bearer ${token}` } }).then(response => {
            let storage: CargaInicial;
            if (response.status == 200) {
                dias.forEach(dia => {
                    if (dia.data == diaSelecionado?.data) {
                        dia.ofertas = request.oferta;
                        dia.quilos = request.kgAmor;
                        dia.visitante = request.visitantes;
                        dia.membros = selectedItems.length;
                    }
                })

                AsyncStorage.getItem(StorageKeys.CARGA_INICIAL).then(r => {
                    storage = JSON.parse(r as any);
                    storage.celulas.forEach(cell => {
                        if (cell.idCelula == idCelulaSelect) {
                            cell.meses.forEach(mes => {
                                if (mes.mesAno == diaSelecionado?.mesAno) {

                                    mes.dias.forEach(dia => {
                                        if (dia.data == diaSelecionado.data) {
                                            //subtrair dia anterior
                                            mes.ofertas -= dia.ofertas;
                                            mes.quilos -= dia.quilos;
                                            mes.visitante -= dia.visitante;

                                            //somar dia atualizado
                                            mes.ofertas += request.oferta;
                                            mes.quilos += request.kgAmor;
                                            mes.visitante += request.visitantes;
                                            mes.membros = request.idsMembros.length < mes.membros ? mes.membros : request.idsMembros.length;

                                            dia.ofertas = request.oferta;
                                            dia.quilos = request.kgAmor;
                                            dia.visitante = request.visitantes;
                                            dia.membros = selectedItems.length;
                                            dia.idsMembros = request.idsMembros;
                                        }
                                    })
                                }
                            })
                        }
                    })
                    AsyncStorage.setItem(StorageKeys.CARGA_INICIAL, JSON.stringify(storage as any));
                });

            }
            setModalVisible(false)
            setLoading(false)
        }).catch(e => { console.log(e) })

    }

    useEffect(() => {
        setIdCelulaSelect(celulas.filter(c => c.id == dias[0].idCelula)[0].id)
    }, [])

    if (!idCelulaSelect) {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (

        <View >
            {dias.map(dia => {
                return (
                    <View style={styles.container}>
                        <View style={styles.evento}>
                            <Text style={styles.eventoText}>
                                <Text style={styles.eventoTextTitle}>Célula: </Text>
                                {filtrarCelula(dia.idCelula).nome}
                            </Text>
                            <Text style={styles.eventoText}>
                                <Text style={styles.eventoTextTitle}>Hórario: </Text>
                                {filtrarCelula(dia.idCelula).horario}
                            </Text>

                            {dia.status
                                ? <Text style={styles.eventoTextOk}>
                                    <Text style={styles.eventoTextTitle}>Relatorio: </Text>
                                    Entregue
                                </Text>
                                : <Text style={styles.eventoTextPendente}>
                                    <Text style={styles.eventoTextTitle}>Relatorio: </Text>
                                    Pendente
                                </Text>
                            }
                        </View>

                        {
                            (dias[0])
                                ? (!dias[0].status) ?
                                    <View>
                                        <TouchableOpacity style={styles.buttonForm} onPress={() => { setModalVisible(true), enviarRelatorio(dia) }}>
                                            <AntDesign name="form" size={20} color="#fff" />
                                            <Text style={{ color: '#ffff', marginLeft: 10, }}>Salvar</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View>
                                        <TouchableOpacity style={styles.buttonEdit} onPress={() => { setModalVisible(true), editarRelatorio(dia) }}>
                                            <AntDesign name="edit" size={20} color="#fff" />
                                            <Text style={{ color: '#ffff', marginLeft: 10, }}>Editar</Text>
                                        </TouchableOpacity>
                                    </View>
                                : null
                        }
                    </View>
                )
            })}

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.modalContainer}>
                    {!loagind ?
                        <View style={styles.modalBody}>
                            <Text style={styles.modalTitle}>Prencher relatório</Text>

                            <View style={styles.modalForm}>
                                <View style={styles.modalTextInputNumber}>
                                    <View style={{ width: '45%' }}>
                                        <Text style={styles.textInputTitle}>Valor Oferta:</Text>
                                        <TextInput
                                            style={styles.modalFormInput}
                                            value={oferta.toString()}
                                            // placeholder='Valor Oferta'
                                            onChangeText={(value) => { setOferta(value) }}
                                            keyboardType='numeric'

                                        />
                                    </View>
                                    <View style={{ width: '45%' }}>
                                        <Text style={styles.textInputTitle}>Quilo do amor:</Text>
                                        <TextInput
                                            style={styles.modalFormInput}
                                            value={quiloAmor.toString()}
                                            // placeholder='Quilo do amor'
                                            onChangeText={(value) => { setQuiloAmor(value) }}
                                            keyboardType='numeric'

                                        />
                                    </View>

                                </View>

                                <Text style={styles.textInputTitle}>Qtd visitantes:</Text>
                                <TextInput
                                    style={styles.modalFormInput}
                                    value={visitante.toString()}
                                    // placeholder='Qtd visitantes'
                                    onChangeText={(value) => { setVisitantes(value) }}
                                    keyboardType='numeric'

                                />

                                <Text style={styles.textInputTitle}>Observação:</Text>
                                <TextInput
                                    style={styles.modalFormInput}
                                    value={observacao}
                                    // placeholder='Qtd visitantes'
                                    onChangeText={(value) => { setObservacao(value) }}

                                />

                                <Text style={styles.listaMembrosTitle}>Membros presentes:</Text>
                                <ScrollView
                                    horizontal
                                    // showsHorizontalScrollIndicator={false}
                                    style={styles.listaItemScroll}
                                >
                                    {filtrarMembrosCelula(idCelulaSelect).membros.map((elem, index) => {
                                        return (
                                            <TouchableOpacity
                                                key={index.toString()}
                                                style={[
                                                    styles.listaMembrosButton,
                                                    selectedItems.includes(elem.id as any) ? styles.selectedItem : {}
                                                ]}
                                                onPress={() => handleSelectItem(elem.id as any)}
                                            >
                                                {selectedItems.includes(elem.id as any) ? (
                                                    <Feather name="user-check" size={24} color="#ffff" />
                                                ) : (
                                                    <Feather name="user-x" size={24} color="black" />
                                                )}
                                                <Text
                                                    style={[
                                                        styles.ItemText,
                                                        selectedItems.includes(elem.id as any) ? styles.selectedItemText : styles.ItemText
                                                    ]}
                                                >
                                                    {elem.apelido}
                                                </Text>
                                            </TouchableOpacity>
                                        )
                                    })}
                                </ScrollView>

                            </View>

                            <View style={styles.modalFooter}>
                                <TouchableOpacity style={styles.modalButtonFechar}
                                    onPress={() => {
                                        setModalVisible(!modalVisible);
                                    }}
                                >
                                    <AntDesign style={{ marginRight: 10 }} name="close" size={18} color="white" />
                                    <Text style={styles.modalFechar}>
                                        Fechar
                                    </Text>
                                </TouchableOpacity>
                                {diaSelecionado?.id ?
                                    <TouchableOpacity style={styles.modalButtonEnviar}
                                        onPress={() => {
                                            handleEditarRelatorioCelula()
                                        }}
                                    >
                                        <AntDesign style={{ marginRight: 10 }} name="rocket1" size={18} color="white" />
                                        <Text style={styles.modalButtonEnviarRel}>
                                            Editar
                                        </Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={styles.modalButtonEnviar}
                                        onPress={() => {
                                            handleRelatorioCelula()
                                        }}
                                    >
                                        <AntDesign style={{ marginRight: 10 }} name="rocket1" size={18} color="white" />
                                        <Text style={styles.modalButtonEnviarRel}>
                                            Enviar
                                        </Text>
                                    </TouchableOpacity>
                                }

                            </View>
                        </View>
                        : <ModalLoading />
                    }

                </View>
            </Modal>

        </View>
    )
}

export default EventoCelula;