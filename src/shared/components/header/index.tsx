import React, { useState, useEffect }  from 'react';
import { View, Text, Image } from 'react-native';

import logoBrancoAlvo from '../../assets/images/logo-alvo-branco.png';

import styles from './styles';
interface HeaderProps {
    title: any
    corRede: string,
}

const Header: React.FC<HeaderProps> = ({ title, corRede }) => {

    useEffect(() => {

    }, [])

    return (
        <View style={[styles.container, {backgroundColor: corRede}]}>
            <View style={styles.topBar}>
                <Text style={styles.title}>{title}</Text>
                <Image source={logoBrancoAlvo} style={styles.imageLogo}/>
            </View>
        </View>
    )
}

export default Header;