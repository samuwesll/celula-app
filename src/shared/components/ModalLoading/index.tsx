import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';

import styles from './styles';

const ModalLoading = () => {
    return (
        <View style={styles.modalBody}>
            <View style={{ flex: 1, justifyContent: 'center', marginTop: 20 }}>
                <ActivityIndicator size="large" color="black" />
                <Text style={{ marginTop: 20 }}>Salvando</Text>
            </View>
        </View>
    )
}

export default ModalLoading;