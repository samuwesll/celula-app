import axios from 'axios';

const api = axios.create({

    baseURL: 'https://alvo-celula-backend.herokuapp.com',

    maxBodyLength: Infinity,
    maxContentLength: Infinity,

    headers: {'Content-Type': 'application/json'}

})

export default api;