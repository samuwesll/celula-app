export interface RelatorioCelulaRequest {
    dataRelatorio: string,
    idCelula: number,
    idLider: number,
    kgAmor: number,
    oferta: number,
    idsMembros: number[],
    visitantes: number,
    observaao: string,
}