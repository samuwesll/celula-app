import { DiasCelula } from "./CargaInicial.model";
import { EventoSemanal } from "./Usuario.model";

export interface EventoProps {
    eventoCelula: boolean,
    eventoSemana?: EventoSemanal[],
    diasCelulas?: DiasCelula[]
}