export interface Sucess<T> {
    mensagem: String,
    value: T,
}

export interface Erros {
    error: String,
    mensagem: String,
}

export interface Response<T> {
    sucesso: Sucess<T>,
    errors: Erros[],
    timestamp: Date,
    status: Number,
}