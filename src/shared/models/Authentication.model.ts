import { Usuario } from "./Usuario.model";

export interface Credenciais {
    login: String,
    senha: String,
}

export interface ResponseAuthentication {
    token: String,
    user: Usuario,
}