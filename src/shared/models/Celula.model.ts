interface Endereco {
    cep: string,
    logradouro: string,
    numero: string,
    complemento: string,
    bairro: string,
    estado: string,
    uf: string,
}

export interface Lider {
    id: number,
    nomeCompleto: string,
    apelido: string,
}

export interface Celula {
    id: number,
    nome: string,
    dia: string,
    horario: string,
    endereco: Endereco,
    lideres: Lider[]
}