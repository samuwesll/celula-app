
interface Rede {
    id: Number,
    cor: String,
    pastores: String,
    stiloCor: string,
}

export interface EventoSemanal {
    diaSemana: number,
    evento: String,
    horario: String,
    nome: String,
    local?: String,
}

export interface Igreja {
    id: Number,
    nome: String,
    estado: String,
    uf: String,
    cidade: String,
    eventoSemanal?: Array<EventoSemanal>,
}

interface Perfil {
    id: Number,
    cargo: String,
}

interface Contato {
    email: String,
    celular: String,
}

export interface Usuario {
    id: number,
    nomeCompleto: string,
    apelido: string,
    login: string,
    dataCadastro: string,
    dataNascimento: string,
    rede: Rede,
    igreja: Igreja,
    perfil: Perfil,
    contato: Contato,
}