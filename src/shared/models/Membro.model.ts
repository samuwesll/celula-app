import { Response } from "./Response.model";

export interface Membro {
    id?: number,
    apelido: string,
    dataNacimento: string,
    nomeCompleto: string,
    nmCelular: string,
    perfil: string,
}

export interface MembroRequestPatch extends Object {
    id?: number,
    apelido?: string,
    dataNacimento?: string,
    nomeCompleto?: string,
    nmCelular?: string,
    perfil?: string,
}

export interface MembroRequest {
    apelido: string,
    celulaId: number,
    dataNascimento: string,
    nmCelular: string,
    nomeCompleto: string,
    perfil: string,
}

export interface CargaMembros {
    idCelula: number,
    membros: Membro[],
}

export interface CargaMembroRS extends Response<CargaMembros[]> {}