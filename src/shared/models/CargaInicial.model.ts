export interface Dia {
    id?: number,
    membros: number;
    visitante: number;
    ofertas: number;
    quilos: number;
    status: boolean;
    nrSemana: number;
    data: string;
    observacao: string;
    idsMembros: Array<number>;
}
export interface DiasCelula {
    id?: number,
    idCelula: number,
    nomeCelula: string;
    mesAno?: string;
    membros?: number;
    visitante?: number;
    ofertas?: number;
    quilos?: number;
    status: boolean;
    nrSemana?: number;
    data: string;
    observacao?: string; 
    idMembros?: Array<number>;
}

export interface Mes {
    mesAno: string;
    membros: number;
    visitante: number;
    ofertas: number;
    quilos: number;
    status: boolean;
    dias: Dia[];
}

export interface Celula {
    nomeCelula: string;
    idCelula: number;
    meses: Mes[];
}

export interface CargaInicial {
    celulas: Array<Celula>
}