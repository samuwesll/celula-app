import React, { useState, useEffect } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons';
import Home from '../pages/home';
import { ActivityIndicator, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StorageKeys } from '../shared/const/storage-keys';
import Membro from '../pages/Membro';
import Ajuste from '../pages/Ajuste';
import Dashboard from '../pages/Dashboard';
import { Usuario } from '../shared/models/Usuario.model';

const { Navigator, Screen } = createBottomTabNavigator();

function LiderTabs() {

    const [corRede, setCorRede] = useState<string>('');
    const [carregamento, setCarregamento] = useState<boolean>(false);

    async function consultaStorage() {

        await AsyncStorage.getItem(StorageKeys.USUARIO).then(r => {
            var usuario: Usuario = JSON.parse(r as any);
            setCorRede(usuario.rede.stiloCor);
        }).catch(e => {});

        setCarregamento(true)
    }

    useEffect(() => {
        consultaStorage();
    }, [])

    if (!carregamento) {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (
        <Navigator
            tabBarOptions={{
                style: {
                    elevation: 0,
                    shadowOpacity: 0,
                    height: 56,
                },
                tabStyle: {
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 8,
                    margin: 4,
                },
                iconStyle: {
                    flex: 0,
                    width: 20,
                    height: 30,
                },
                labelStyle: {
                    fontSize: 13,
                    fontWeight: 'bold',
                },
                activeTintColor: `${corRede}`
            }}
        >
            <Screen
                name="Home"
                component={() => Home({
                    corRede: `${corRede}`
                })}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="home" size={24} color={color} />
                    }
                }}
            />
            <Screen
                name="Membros"
                component={() => Membro({ corRede: `${corRede}` })}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="team" size={24} color={color} />
                    }
                }}
            />
            <Screen
                name="Relatórios"
                component={() => Dashboard({ 
                    corTheme: corRede
                })}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="piechart" size={24} color={color} />
                    }
                }}
            />
            <Screen
                name="Usuário"
                component={() => Ajuste({ corRede: `${corRede}` })}
                options={{
                    tabBarIcon: ({ color, size, focused }) => {
                        return <AntDesign name="solution1" size={24} color={color} />
                    }
                }}
            />
        </Navigator>
    )
}

export default LiderTabs;