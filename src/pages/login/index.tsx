import React, { useState, useEffect } from 'react';
import { 
    ScrollView, ImageBackground, View, Image, Text, TextInput,
    TouchableOpacity, ActivityIndicator, Alert} 
from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Feather } from '@expo/vector-icons';
import api from '../../shared/services/api';

import fundoImage from '../../shared/assets/images/fundo_logo.png';
import logoImage from '../../shared/assets/images/logo.png';
import styles from './styles';
import { ResponseAuthentication } from '../../shared/models/Authentication.model';
import { Response } from '../../shared/models/Response.model';
import { useNavigation } from '@react-navigation/native';
import { Celula } from '../../shared/models/Celula.model';
import { CargaInicial } from '../../shared/models/CargaInicial.model';
import { CargaMembroRS } from '../../shared/models/Membro.model';
import { StorageKeys } from '../../shared/const/storage-keys';

function Login() {

    const [name, setName] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [carregando, setCarregando] = useState<boolean>(true)

    const navigation = useNavigation();

    async function login(login: String, senha: String) {
        setCarregando(true)
        let loginAuth: Response<ResponseAuthentication>;
        
        await api.post<Response<ResponseAuthentication>>('auth', {login, senha}).then(response => {
            loginAuth = response.data;
            const { token, user } = loginAuth.sucesso.value;
            AsyncStorage.setItem(StorageKeys.TOKEN, JSON.stringify(token))
            AsyncStorage.setItem(StorageKeys.USUARIO, JSON.stringify(user))
            AsyncStorage.setItem(StorageKeys.IGREJA, JSON.stringify(user.igreja))
            cargaInicial(token);
        }).catch(error => {
            setCarregando(false)
            const { status, errors } = error.response.data;
            Alert.alert(`Erro ${status}`, `${errors[0].mensagem}`)
            return
        })

    }

    async function cargaInicial(token: String) {
        let celulasResponse: Response<Celula[]>;
        await api.get('v1/celula/login', {headers: { 'Authorization' : `Bearer ${token}`}}).then(response => {
            celulasResponse = response.data;
            const { value } =  celulasResponse.sucesso;
            if (value == null) {
                setCarregando(false)
                Alert.alert(`Erro 404`, `Nenhuma celula vinculada com o usuario`);
                return
            } else {
                AsyncStorage.setItem(StorageKeys.LISTA_CELULAS, JSON.stringify(value))
                
            }
        }).catch(erro => {
            setCarregando(false)
            const { status, errors } = erro.response.data;
            Alert.alert(`Erro ${status}`, `${errors[0].mensagem}`)
            return
        })

        await api.get<Response<CargaInicial>>('v1/app/carga-inicial', { headers: { 'Authorization' : `Bearer ${token}`}}).then(response => {
            let cargaInicial: Response<CargaInicial>;
            cargaInicial = response.data;
            AsyncStorage.setItem(StorageKeys.CARGA_INICIAL, JSON.stringify(cargaInicial.sucesso.value,null, Infinity))
        }).catch(erro => {
            setCarregando(false)
            const { status, errors } = erro.response.data;
            Alert.alert(`Erro ${status}`, `${errors[0].mensagem}`)
            return
        })

        await api.get<CargaMembroRS>('v1/membro/buscaPorUser', { headers: { 'Authorization' : `Bearer ${token}`}}).then(res => {
            const membrosCarga = res.data.sucesso.value;
            AsyncStorage.setItem(StorageKeys.CARGA_MEMBROS, JSON.stringify(membrosCarga));
            setCarregando(false)
            navigation.navigate('PageLider')
        }) 
    }

    async function consultarStorage() {
        await AsyncStorage.getItem(StorageKeys.TOKEN).then(r => {
            let texto: string = JSON.parse(r as any)
            if (texto != null) {
                navigation.navigate('PageLider')
            }
        }).catch(r => {})
    }
    
    useEffect(() => {
        consultarStorage();
        setCarregando(false)
    },[])

    if (carregando) {
        return (
            <View style={styles.carrregamento}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <ImageBackground source={fundoImage} 
                imageStyle={{ width: 274, height: 368 }}
                style={styles.container}
            >
                <View style={styles.main}>
                    <Image source={logoImage} />
                    <Text style={styles.title}>Uma igreja para quem pensa.</Text>
                    <Text style={styles.description}>Relatório de célula</Text>
                </View>

                <View>
                    <TextInput 
                        style={styles.input} value={name} 
                        placeholder='Digite seu login' onChangeText={setName} maxLength={20} 
                    />
                    <TextInput 
                        secureTextEntry={true} value={password} style={styles.input} 
                        placeholder='Digite senha' onChangeText={setPassword} maxLength={30}
                    />
                    <TouchableOpacity
                        disabled={false}
                        style={styles.button}
                        onPress={() => login(name, password)}
                    >
                        <View style={styles.buttonIcon}>
                            <Feather name="arrow-right" color="#fff" size={24} />
                        </View>
                        <Text style={styles.buttonText}>Entrar</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </ScrollView>
    );
}

export default Login