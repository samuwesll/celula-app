import React, { useState, useEffect } from 'react';
import { ActivityIndicator, View } from 'react-native';
import Calendario from '../../shared/components/Calendario';
import { CargaInicial, DiasCelula } from '../../shared/models/CargaInicial.model';
import { Celula } from '../../shared/models/Celula.model';
import { CargaMembros } from '../../shared/models/Membro.model';
import AsyncStorage from '@react-native-async-storage/async-storage';

import styles from './styles';
import { StorageKeys } from '../../shared/const/storage-keys';
import { useFocusEffect } from '@react-navigation/native';
import { Igreja } from '../../shared/models/Usuario.model';

interface DashboardProps {
    corTheme: string,
}

const Dashboard: React.FC<DashboardProps> = ({ corTheme }) => {

    const [carga, setCarga] = useState<CargaInicial>();
    const [diasCelula, setDiasCelula] = useState<DiasCelula[]>([]);
    const [carregamento, setCarregamento] = useState<boolean>(false);
    const [cargaMembro, setCargaMembros] = useState<CargaMembros[]>([]);
    const [igreja, setIgreja] = useState<Igreja>();
    const [celulas, setCelulas] = useState<Celula[]>([]);

    async function consultaStorage() {

        await AsyncStorage
            .multiGet([StorageKeys.CARGA_INICIAL, StorageKeys.CARGA_MEMBROS, StorageKeys.LISTA_CELULAS, StorageKeys.IGREJA])
            .then(([cargaStorage, membrosStorage, celulasStorage, igrejaStorage]) => {
                const storage: CargaInicial = JSON.parse(cargaStorage[1] as any)
                let dias: DiasCelula[] = [];
                setCarga(storage)
                storage.celulas.forEach(cell => {
                    cell.meses.forEach(mes => {
                        mes.dias.forEach(dia => {
                            if (dia.status) {
                                let diaRel: DiasCelula = {
                                    id: dia.id,
                                    data: dia.data,
                                    membros: dia.membros,
                                    nrSemana: dia.nrSemana,
                                    observacao: dia.observacao,
                                    ofertas: dia.ofertas,
                                    quilos: dia.quilos,
                                    status: dia.status,
                                    visitante: dia.visitante,
                                    nomeCelula: cell.nomeCelula,
                                    idCelula: cell.idCelula,
                                    mesAno: mes.mesAno,
                                    idMembros: dia.idsMembros,
                                }
                                dias.push(diaRel);
                            }
                        })
                    })
                })
                setDiasCelula(dias);

                setCargaMembros(JSON.parse(membrosStorage[1] as any));
                setCelulas(JSON.parse(celulasStorage[1] as any));
                setIgreja(JSON.parse(igrejaStorage[1] as any));
            }).catch(e => { })

        setCarregamento(true);
    }

    useEffect(() => {
        consultaStorage();
    }, [])

    useEffect(() => {
        consultaStorage();
    }, [corTheme])

    if (!carregamento) {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <Calendario
                corTheme={corTheme}
                diasRelatorio={diasCelula}
                celulas={celulas}
                cargaMembros={cargaMembro}
                church={igreja as any}
            />
        </View>
    )
}

export default Dashboard;