import { StyleSheet } from "react-native";
import Constants from 'expo-constants';

const styles = StyleSheet.create({
    carrregamento: {
        flex: 1,
        justifyContent: 'center',
    },
    container: {
        flex: 1,
    },
    headerPicker: {
        flexDirection: 'row',
    },
    pickers: {
        width: '50%',
    },
    descricaoCelula: {
        alignSelf: 'center',
        width: '95%',
        display: "flex",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 2,
        padding: 10,
        marginTop: 10,
    },
    detalhesCelulas: {
        marginTop: 20,
        padding: 10,
        backgroundColor: '#ffff',
        borderRadius: 16,
    },
    detalhesCelulasHeader: {
        alignItems: 'center',
        marginBottom: 10,
    },
    detalhesCelulasHeaderText: {
        fontSize: 16,
        fontFamily: 'Roboto_500Medium'
    },

    infoCelula: {
        marginTop: 5,
        marginBottom: 10,
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
    infoPart: {
        alignItems: 'center',
        width: '30%',
        borderWidth: 1,
        borderColor: "#dcdc",
        borderRadius: 6,
    },
    infoPartTitle: {
        fontWeight: 'bold',
    },
    infoPartText: {
        marginTop: 3,
        marginBottom: 5,
    },
})

export default styles;