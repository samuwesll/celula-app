import React, { useEffect, useState } from 'react';
import { Text, ScrollView, View, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Picker } from '@react-native-picker/picker';

import Header from '../../shared/components/header';
import Chart from '../../shared/components/chart';

import styles from './styles';
import { Celula } from '../../shared/models/Celula.model';
import { CargaInicial, Celula as CargaItem, Mes } from '../../shared/models/CargaInicial.model';
import { StorageKeys } from '../../shared/const/storage-keys';
import { CargaMembros, Membro } from '../../shared/models/Membro.model';
import { UtilsHelper } from '../../shared/helpers/util.helper';

interface HomeProps {
    corRede: string,
}

const Home: React.FC<HomeProps> = ({ corRede }) => {

    const [infoSemana, setInfoSemanas] = useState<any[]>([]);
    const [infoSemanaIndex, setInfoSemanasIndex] = useState<any[]>([]);
    const [celulas, setCelulas] = useState<Celula[]>([]);
    const [cargaItem, setCargaItem] = useState<CargaItem>();
    const [selectedCelula, setSelectedCelula] = useState<string>();
    const [selectedMes, setSelectedMes] = useState<string>();
    const [mes, setMes] = useState<Mes>();
    const [carga, setCarga] = useState<CargaInicial>();
    const [carregamento, setCarregamento] = useState<boolean>(false);
    const [cargaMembros, setCargaMembros] = useState<CargaMembros[]>([]);

    const [membrosSelecionados, setMembrosSelecionados] = useState<Membro[]>([]);

    function mascaraMesAno(mesAno: string) {
        const dataString: string[] = mesAno.split('-')
        var data;

        switch (Number.parseInt(dataString[1])) {
            case 1:
                data = `jan/${dataString[0]}`;
                break
            case 2:
                data = `fev/${dataString[0]}`;
                break
            case 3:
                data = `mar/${dataString[0]}`;
                break
            case 4:
                data = `abr/${dataString[0]}`;
                break
            case 5:
                data = `mai/${dataString[0]}`;
                break
            case 6:
                data = `jun/${dataString[0]}`;
                break
            case 7:
                data = `jul/${dataString[0]}`;
                break
            case 8:
                data = `ago/${dataString[0]}`;
                break
            case 9:
                data = `set/${dataString[0]}`;
                break
            case 10:
                data = `out/${dataString[0]}`;
                break
            case 11:
                data = `nov/${dataString[0]}`;
                break
            case 12:
                data = `dez/${dataString[0]}`;
                break
        }

        return data
    }

    function aniversariantesMes(date: string) {
        let anivers: Membro[] = [];
        const cell = celulas.filter(c => c.nome == selectedCelula)[0];
        if (date && selectedCelula) {
            const mes = date.split("-")
            cargaMembros.forEach(carga => {
                if (carga.idCelula == cell.id) {
                    carga.membros.forEach(m => {
                        var mesMemb = m.dataNacimento.split('-');
                        if (mes[1] == mesMemb[1]) {
                            anivers.push(m);
                        }
                    })
                }
            })
        }
        setMembrosSelecionados(anivers);
    }

    function consultarStorage() {
        const { LISTA_CELULAS, CARGA_INICIAL, CARGA_MEMBROS } = StorageKeys;
        AsyncStorage.multiGet([LISTA_CELULAS, CARGA_INICIAL, CARGA_MEMBROS]).then(([cellStorage, cargaStorage, membrosStorage]) => {
            setCelulas(JSON.parse(cellStorage[1] as any));
            setCarga(JSON.parse(cargaStorage[1] as any));
            setCargaMembros(JSON.parse(membrosStorage[1] as any));
        })
        setCargaItem(carga?.celulas[0]);
        setCarregamento(true)
    }

    const selecionarMes = (index: number) => {
        setMes(cargaItem?.meses[index])
        aniversariantesMes(cargaItem?.meses[index].mesAno as any);
    }

    function selecionarCelula(index: number)  {
        setCargaItem(carga?.celulas[index]);
        // setMes(cargaItem?.meses[0])
        // setSelectedMes(cargaItem?.meses[0].mesAno);
        // montarInfoDas();
    }

    const montarInfoDas = () => {
        var infSem: any[] = [];
        var semIndex: any[] = [];
        mes?.dias.forEach(d => {
            if (d.visitante + d.membros != 0) {
                infSem.push([d.visitante, d.membros])
                semIndex.push(`${d.nrSemana} sem`)
            }
        })
        setInfoSemanas(infSem);
        setInfoSemanasIndex(semIndex)
    }


    useEffect(() => {
        consultarStorage();
    }, [])

    useEffect(() => {
        setMes(cargaItem?.meses[0])
        setSelectedMes(cargaItem?.meses[0].mesAno);
        montarInfoDas();
    }, [cargaItem])
    
    useEffect(() => {
        montarInfoDas();
    }, [mes])

    useEffect(() => {
        montarInfoDas();
        aniversariantesMes(cargaItem?.meses[0].mesAno as any);
    }, [selectedCelula])

    if (!carregamento) {
        return (
            <View style={styles.carrregamento}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }


    return (
        <SafeAreaView style={styles.container}>
            <Header title={'Gráficos células'} corRede={`${corRede}`} />

            <ScrollView>
                <View style={styles.descricaoCelula}>
                    <View style={styles.headerPicker}>
                        <Picker
                            style={styles.pickers}
                            selectedValue={selectedCelula}
                            onValueChange={(itemValue, itemIndex) => {
                                setSelectedCelula(itemValue), selecionarCelula(itemIndex)
                            }}
                        >
                            {carga?.celulas.map(c => {
                                return (
                                    <Picker.Item key={c.idCelula} label={c.nomeCelula} value={c.nomeCelula} />
                                )
                            })}
                        </Picker>

                        <Picker
                            style={styles.pickers}
                            selectedValue={selectedMes}
                            onValueChange={(itemValue, itemIndex) => {
                                setSelectedMes(itemValue), selecionarMes(itemIndex)
                            }
                            }>
                            {cargaItem?.meses.map((c, index) => {
                                return (
                                    <Picker.Item key={index + 1} label={mascaraMesAno(c.mesAno)} value={c.mesAno} />
                                )
                            })}
                        </Picker>

                    </View>

                    <Chart semanas={infoSemana as any} semanasIndex={infoSemanaIndex as any} />

                    <View style={styles.detalhesCelulas}>
                        <View style={styles.detalhesCelulasHeader}>
                            <Text style={styles.detalhesCelulasHeaderText}>Detalhes:</Text>
                        </View>

                        <View style={styles.infoCelula}>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Visitantes:</Text>
                                <Text style={styles.infoPartText}>{mes?.visitante.toString().padStart(2, '0')}</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Membros:</Text>
                                <Text style={styles.infoPartText}>{mes?.membros.toString().padStart(2, '0')}</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Total Pessoas</Text>
                                <Text style={styles.infoPartText}>{(mes?.membros as any + mes?.visitante).toString().padStart(2, '0')}</Text>
                            </View>
                        </View>
                        <View style={styles.infoCelula}>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Kg do amor:</Text>
                                <Text style={styles.infoPartText}>{mes?.quilos.toFixed(3)} kg</Text>
                            </View>
                            <View style={styles.infoPart}>
                                <Text style={styles.infoPartTitle}>Ofertas:</Text>
                                <Text style={styles.infoPartText}>R$ {(mes?.ofertas.toFixed(2))}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.detalhesCelulas}>
                        <View style={styles.detalhesCelulasHeader}>
                            <Text style={styles.detalhesCelulasHeaderText}>Aniversariantes do mês:</Text>
                        </View>

                        {membrosSelecionados.map((membro: Membro, index) => {
                            return (
                                <View key={index} style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text>
                                        <Text style={{ fontWeight: 'bold' }}>Apelido: </Text>
                                        {membro.apelido}
                                    </Text>
                                    <Text>
                                        <Text style={{ fontWeight: 'bold' }}>Data: </Text>
                                        {UtilsHelper.dataAniversario(membro.dataNacimento)}
                                    </Text>
                                </View>
                            )
                        })}

                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default Home;