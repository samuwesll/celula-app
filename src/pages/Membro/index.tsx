import React, { useState, useEffect } from 'react';
import { ActivityIndicator, Alert, Linking, Modal, Platform, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';

import PageHeader from '../../shared/components/header';
import { CargaMembros, Membro as Memb, MembroRequest, MembroRequestPatch } from '../../shared/models/Membro.model';

import { AntDesign, FontAwesome5 } from '@expo/vector-icons';
import styles from './styles';
import { UtilsHelper } from '../../shared/helpers/util.helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StorageKeys } from '../../shared/const/storage-keys';
import { Celula } from '../../shared/models/Celula.model';
import api from '../../shared/services/api';
import { Response } from '../../shared/models/Response.model';
import ModalLoading from '../../shared/components/ModalLoading';

interface MembroProps {
    corRede: string,
}

const Membro: React.FC<MembroProps> = ({ corRede }) => {

    const [membros, setMembro] = useState<Memb[]>();
    const [selectedCelula, setSelectedCelula] = useState<string>();
    const [celulas, setCelulas] = useState<Celula[]>([]);
    const [token, setToken] = useState<string>('');
    const [cargaMembros, setCargaMembros] = useState<CargaMembros[]>([]);
    const [carregamento, setCarregamento] = useState<boolean>(false);
    const [perfilMembro, setPerfilMembro] = useState<string>('ADULTO')

    const [formValido, setFormValido] = useState<boolean>(false);

    const [modalVisible, setModalVisible] = useState(false);
    const [textNome, setTextNome] = useState<string>('')
    const [textApelido, setTextApelido] = useState<string>('')
    const [textTelefone, setTextTelefone] = useState<string>('')
    const [textDateString, setTextDateString] = useState<string>('')
    const [selectMembroEdicao, setSelectMembroEdicao] = useState<Memb>();
    const [flEdicao, setFlEdicao] = useState<boolean>(false);
    const [modalDeletar, setModalDeletar] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [idMembroDeletar, setIdMembroDeletar] = useState<number>();

    function enviarMensagem(mem: Memb) {
        Linking.openURL(`whatsapp://send?phone=+55${mem.nmCelular}`)
    }

    async function editarMembroReq() {
        let membroPatch = verificarAlteracaoAlt();
        setLoading(true);

        if (membroPatch.nomeCompleto || membroPatch.nmCelular || 
            membroPatch.apelido || membroPatch.dataNacimento || membroPatch.perfil) {
            var cell = celulas.filter(c => c.nome == selectedCelula);
            const celulaId = cell[0].id;

            membroPatch.id = selectMembroEdicao?.id;

            await api.patch<Response<String>>("v1/membro", membroPatch, { headers: { 'Authorization': `Bearer ${token}` } }).then(resp => {
                if (resp.status == 200) {
                    cargaMembros.forEach(c => {
                        if (c.idCelula == celulaId) {
                            c.membros.forEach(m => {
                                if (m.id == membroPatch.id) {
                                    m.apelido = membroPatch.apelido ? membroPatch.apelido : m.apelido;
                                    m.nomeCompleto = membroPatch.nomeCompleto ? membroPatch.nomeCompleto : m.nomeCompleto;
                                    m.dataNacimento = membroPatch.dataNacimento ? membroPatch.dataNacimento : m.dataNacimento;
                                    m.nmCelular = membroPatch.nmCelular ? membroPatch.nmCelular : m.nmCelular;
                                    m.perfil = membroPatch.perfil ? membroPatch.perfil : m.perfil;
                                }
                            })
                            setMembro(c.membros)
                        }
                    })
                    AsyncStorage.setItem(StorageKeys.CARGA_MEMBROS, JSON.stringify(cargaMembros))
                }


            }).catch(e => {
                // setModalVisible(false)
            }).finally(() => { setModalVisible(false), setLoading(false) })

        } else {
            console.log("NÃO ENTROU")
            setModalVisible(false)
            setLoading(false);
        }

    }

    function verificarAlteracaoAlt(): MembroRequestPatch {
        var memb: MembroRequestPatch = {};
        const nmCelular = textTelefone.replace(/\D/g, "");
        const dataNas = textDateString.split("/");
        const dataNascimento = `${dataNas[2]}-${dataNas[1]}-${dataNas[0]}`

        if (selectMembroEdicao?.nomeCompleto != textNome) {
            memb.nomeCompleto = textNome;
        }

        if (selectMembroEdicao?.apelido != textApelido) {
            memb.apelido = textApelido;
        }

        if (selectMembroEdicao?.nmCelular != nmCelular) {
            memb.nmCelular = nmCelular;
        }

        if (selectMembroEdicao?.dataNacimento != dataNascimento) {
            memb.dataNacimento = dataNascimento;
        }

        if (UtilsHelper.buscarEnumPerfilMembro(selectMembroEdicao?.perfil as any) != perfilMembro) {
            memb.perfil = perfilMembro;
        }

        return memb;
    }

    async function salvarMembro() {
        setLoading(true);
        var cell = celulas.filter(c => c.nome == selectedCelula);
        const celulaId = cell[0].id;
        const nmCelular = textTelefone.replace(/\D/g, "");
        const dataNas = textDateString.split("/");
        const dataNascimento = `${dataNas[2]}-${dataNas[1]}-${dataNas[0]}`

        const request: MembroRequest = {
            celulaId,
            apelido: textApelido,
            nomeCompleto: textNome,
            nmCelular,
            dataNascimento,
            perfil: perfilMembro
        }

        await api.post<Response<number>>("v1/membro", request, { headers: { 'Authorization': `Bearer ${token}` } }).then(response => {
            if (response.status == 201) {
                const membro: Memb = {
                    id: response.data.sucesso.value,
                    apelido: request.apelido,
                    nmCelular: request.nmCelular,
                    nomeCompleto: request.nomeCompleto,
                    dataNacimento: request.dataNascimento,
                    perfil: request.perfil,
                }
                cargaMembros.forEach(carga => {
                    if (carga.idCelula == cell[0].id) {
                        carga.membros.push(membro)
                        setMembro(carga.membros)
                    }
                })

                AsyncStorage.setItem(StorageKeys.CARGA_MEMBROS, JSON.stringify(cargaMembros))
                setModalVisible(false)
                setLoading(false);
            }
        }).then(error => {
            setModalVisible(false)
            setLoading(false);
        })
    }

    function novoIntegrante() {
        setSelectMembroEdicao(null as any);
        setTextTelefone('');
        setTextNome('');
        setTextApelido('');
        setTextTelefone('');
        setTextDateString('');
        setFlEdicao(false)
        setFormValido(false)
    }

    const filtrarMembrosPorCelula = (index: number) => {
        setMembro(cargaMembros[index].membros);
    }

    const mascaraTelefone = (num: string) => {
        var r = num.replace(/\D/g, "");
        r = r.replace(/^0/, "");
        if (r.length > 10) {
            r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (r.length > 5) {
            r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (r.length > 2) {
            r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else {
            r = r.replace(/^(\d*)/, "($1");
        }
        setTextTelefone(r);
        validarFormulario();
    }

    const mascaraDataNascimento = (data: string) => {
        var r = data.replace(/\D/g, "");
        if (r.length > 8) {
            r = r.replace(/^(\d\d)(\d\d)(\d{4}).*/, "$1/$2/$3");
        } else if (r.length > 4) {
            r = r.replace(/^(\d\d)(\d\d)(\d{0,4}).*/, "$1/$2/$3")
        } else if (r.length > 2) {
            r = r.replace(/^(\d\d)(\d{0,2}).*/, "$1/$2")
        } else {
            r = r.replace(/^(\d*)/, "$1");
        }
        setTextDateString(r)
        validarFormulario();
    }

    const validarFormulario = () => {
        const dataNas = textDateString?.replace(/\D/g, "");
        const telefone = textTelefone.replace(/\D/g, "");
        const nome = textNome.split("");
        const apelido = textApelido.split("")

        if (
            dataNas.length >= 7 &&
            telefone.length >= 10 &&
            nome.length > 5 &&
            apelido.length > 4
        ) {
            setFormValido(true)
        } else {
            setFormValido(false)
        }
    }

    const editarMembro = (membro: Memb) => {
        setSelectMembroEdicao(membro);
        setFlEdicao(true)
        var date = membro.dataNacimento.split("-");
        setTextNome(membro.nomeCompleto);
        setTextApelido(membro.apelido);
        setTextTelefone('');
        setTextDateString('');
        mascaraTelefone(membro.nmCelular);
        mascaraDataNascimento(`${date[2]}${date[1]}${date[0]}`)
        setModalVisible(true)
        setFormValido(false)
        setPerfilMembro(UtilsHelper.buscarEnumPerfilMembro(membro.perfil))
    }

    function consultaStorage() {
        AsyncStorage.multiGet([StorageKeys.LISTA_CELULAS, StorageKeys.TOKEN, StorageKeys.CARGA_MEMBROS]).then(([a, b, c]) => {
            setCelulas(JSON.parse(a[1] as any));
            setToken(JSON.parse(b[1] as any));
            setCargaMembros(JSON.parse(c[1] as any));
            filtrarMembrosPorCelula(0);
        }).catch(e => { });
        setCarregamento(true)
    }

    function deletar(idMembro: number) {
        setModalDeletar(true)

        setIdMembroDeletar(idMembro);
    }

    async function confirmarExclusao() {
        setLoading(false);
        var cell = celulas.filter(c => c.nome == selectedCelula);

        api.delete<Response<null>>(`v1/membro/${idMembroDeletar}`, { headers: { 'Authorization': `Bearer ${token}` } }).then(response => {
            if (response.status == 200) {
                cargaMembros.forEach(carga => {
                    if (carga.idCelula == cell[0].id) {
                        carga.membros.forEach((m, i) => {
                            if (m.id == idMembroDeletar) {
                                carga.membros.splice(i, 1)
                            }
                        })

                    }
                })
                AsyncStorage.removeItem(StorageKeys.CARGA_MEMBROS);
                AsyncStorage.setItem(StorageKeys.CARGA_MEMBROS, JSON.stringify(cargaMembros));
            }
        }).catch(e => { })
            .finally(() => {
                setModalDeletar(false);
                setLoading(false);
            })
    }

    useEffect(() => {
        consultaStorage();
    }, [])

    if (!carregamento) {
        return (
            <View style={styles.carrregamento}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <PageHeader title={"Membros"} corRede={`${corRede}`} />

            <View style={styles.headerPicker}>
                <Picker
                    style={styles.pickers}
                    selectedValue={selectedCelula}
                    onValueChange={(itemValue, itemIndex) => {
                        setSelectedCelula(itemValue), filtrarMembrosPorCelula(itemIndex)
                    }
                    }>
                    {celulas.map(c => {
                        return (
                            <Picker.Item key={c.id} label={c.nome} value={c.nome} />
                        )
                    })}
                </Picker>
            </View>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>{`Integrantes da célula:`}</Text>
            </View>

            <ScrollView>
                <View style={styles.bodyCard}>
                    {membros?.map(membro => {
                        return (
                            <View key={membro.id} style={styles.styleCardMembro}>
                                <View>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Nome: </Text>
                                        {membro.nomeCompleto}
                                    </Text>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Apelido: </Text>
                                        {membro.apelido}
                                    </Text>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Perfil: </Text>
                                        {!membro.perfil ? 'N/A' : membro.perfil}
                                    </Text>
                                    <Text style={styles.styleCardMembroText}>
                                        <Text style={styles.styleCardMembroTextTitle}>Data Aniversario: </Text>
                                        {UtilsHelper.dataAniversario(membro.dataNacimento as string)}
                                    </Text>
                                    <View style={styles.containerButton}>
                                        <TouchableOpacity style={styles.buttonWhastApp} onPress={() => { enviarMensagem(membro) }}>
                                            <FontAwesome5 name="whatsapp" size={23} color="#fff" />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.buttonEdit} onPress={() => { editarMembro(membro) }}>
                                            <AntDesign name="edit" size={24} color="#fff" />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.buttonDelete} onPress={() => { deletar(membro.id as any) }}>
                                            <AntDesign name="delete" size={22} color="#fff" />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        )
                    })}
                    <View style={styles.footer}>
                        <TouchableOpacity style={styles.footerButton} onPress={() => { novoIntegrante(), setModalVisible(true) }}>
                            <AntDesign name="addusergroup" size={26} color="#fff" />
                            <Text style={styles.footerButtonText}>Novo Integrantes</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView>
            <Modal
                animationType='slide'
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.modalContainer}>
                    {!loading ?
                        <View style={styles.modalBody}>
                            <Text style={styles.modalTitle}>{'Novo Integrante na \n célula ' + selectedCelula}</Text>

                            <View style={styles.modalForm}>
                                <Text style={styles.textInputTitle}>Nome: </Text>
                                <TextInput
                                    style={styles.modalFormInput}
                                    value={textNome}
                                    placeholder="Fala Querido"
                                    onChangeText={(valor) => { setTextNome(valor), validarFormulario() }}
                                />

                                <View style={styles.cardRow}>
                                    <View style={{ width: '48%', marginRight: 4 }}>
                                        <Text style={styles.textInputTitle}>Apelido: </Text>
                                        <TextInput
                                            style={styles.modalFormInput}
                                            value={textApelido}
                                            placeholder="Querido"
                                            onChangeText={(valor) => { setTextApelido(valor), validarFormulario() }}
                                        />
                                    </View>
                                    <View style={{ width: '70%' }}>
                                        <Text style={styles.textInputTitle}> </Text>
                                        <Picker style={styles.pickersPerfil}
                                            selectedValue={perfilMembro}
                                            itemStyle={{ fontSize: 16, }}
                                            onValueChange={(itemValue, itemIndex) => {
                                                setPerfilMembro(itemValue), validarFormulario()
                                            }}>
                                            <Picker.Item key={1} label={'ADULTO'} value={'ADULTO'} />
                                            <Picker.Item key={1} label={'CRIANÇA'} value={'CRIANCA'} />
                                        </Picker>
                                    </View>
                                </View>

                                <Text style={styles.textInputTitle}>Telefone: </Text>
                                <TextInput
                                    style={styles.modalFormInput}
                                    value={textTelefone}
                                    onChangeText={(valor) => { mascaraTelefone(valor) }}
                                    placeholder="(00) 00000-0000"
                                    keyboardType='numeric'

                                />

                                <Text style={styles.textInputTitle}>Data Nascimento: </Text>
                                <TextInput
                                    style={styles.modalFormInput}
                                    value={textDateString}
                                    placeholder="01/12/1990"
                                    keyboardType='numeric'
                                    onChangeText={(valor) => { mascaraDataNascimento(valor) }}
                                />
                            </View>

                            <View style={styles.modalFooter}>

                                <TouchableOpacity style={styles.modalButtonFechar}
                                    onPress={() => { setModalVisible(!modalVisible) }}
                                >
                                    <AntDesign style={{ marginRight: 10 }} name="close" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        Fechar
                                    </Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={!formValido ? styles.modalButtonEnviarInativo : styles.modalButtonEnviarAtivo}
                                    disabled={!formValido} onPress={() => { !flEdicao ? salvarMembro() : editarMembroReq() }}
                                >
                                    <AntDesign style={{ marginRight: 10 }} name="save" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        {!flEdicao ? 'Cadastrar' : 'Editar'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        : <ModalLoading />
                    }

                </View>
            </Modal>

            <Modal
                animationType='slide'
                transparent={true}
                visible={modalDeletar}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.modalContainer}>
                    {!loading ?
                        <View style={styles.modalBody}>
                            <Text style={styles.modalTitle}>Deletar membro</Text>
                            <View style={styles.modalInfo}>
                                <Text>Tem certeza que deseja deletar o membro Laranja?</Text>
                            </View>

                            <View style={styles.modalFooter}>
                                <TouchableOpacity style={styles.modalButtonFecharTwo} onPress={() => { setModalDeletar(!modalDeletar) }}>
                                    <AntDesign style={{ marginRight: 10 }} name="dislike2" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        Não
                                    </Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.modalButtonOk} onPress={() => { confirmarExclusao() }}>
                                    <AntDesign style={{ marginRight: 10 }} name="like2" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        Sim
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        : <ModalLoading />
                    }

                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default Membro