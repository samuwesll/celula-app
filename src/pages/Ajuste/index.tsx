import React, { useState, useEffect } from 'react';
import { View, Text, SafeAreaView, ScrollView, Modal, Alert, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import PageHeader from '../../shared/components/header';

import styles from './styles';
import { Usuario } from '../../shared/models/Usuario.model';
import { StorageKeys } from '../../shared/const/storage-keys';
import { UtilsHelper } from '../../shared/helpers/util.helper';
import { useNavigation } from '@react-navigation/native';
import { Celula } from '../../shared/models/Celula.model';
import ModalLoading from '../../shared/components/ModalLoading';
import { Response } from '../../shared/models/Response.model';
import api from '../../shared/services/api';
import { CargaInicial } from '../../shared/models/CargaInicial.model';
import { CargaMembroRS } from '../../shared/models/Membro.model';

interface AjusteProps {
    corRede: string,
}

const Ajuste: React.FC<AjusteProps> = ({ corRede }) => {

    const [usuario, setUsuario] = useState<Usuario>();
    const [celulas, setCeluas] = useState<Celula[]>();
    const [carregamento, setCarregamento] = useState<boolean>(false);
    const [modalVisibleSair, setModalVisibleSair] = useState(false);
    const [modalAtualizacao, setModalAtualizacao] = useState(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [token, setToken] = useState<string>('');

    const navigation = useNavigation();

    function consultarStorage() {
        AsyncStorage.multiGet([StorageKeys.USUARIO, StorageKeys.LISTA_CELULAS, StorageKeys.TOKEN]).then(([userStorage, cellStorage, token]) => {
            setUsuario(JSON.parse(userStorage[1] as any));
            setCeluas(JSON.parse(cellStorage[1] as any));
            setToken(JSON.parse(token[1] as any));
        }).catch(e => { })
        setCarregamento(true)
    }

    function logoff() {
        AsyncStorage.clear().then(response => {
            navigation.navigate('Login')
        })
    }

    async function baixarInformacoes() {
        setLoading(true);

        let celulasResponse: Response<Celula[]>;
        await api.get('v1/celula/login', {headers: { 'Authorization' : `Bearer ${token}`}}).then(response => {
            celulasResponse = response.data;
            const { value } =  celulasResponse.sucesso;
            if (value == null) {
                Alert.alert(`Erro 404`, `Nenhuma celula vinculada com o usuario`);
                return
            } else {
                AsyncStorage.setItem(StorageKeys.LISTA_CELULAS, JSON.stringify(value))
                
            }
        }).catch(erro => {
            const { status, errors } = erro.response.data;
            Alert.alert(`Erro ${status}`, `${errors[0].mensagem}`)
            return
        })

        await api.get<Response<CargaInicial>>('v1/app/carga-inicial', { headers: { 'Authorization' : `Bearer ${token}`}}).then(response => {
            let cargaInicial: Response<CargaInicial>;
            cargaInicial = response.data;
            AsyncStorage.setItem(StorageKeys.CARGA_INICIAL, JSON.stringify(cargaInicial.sucesso.value,null, Infinity))
        }).catch(erro => {
            const { status, errors } = erro.response.data;
            Alert.alert(`Erro ${status}`, `${errors[0].mensagem}`)
            return
        })

        await api.get<CargaMembroRS>('v1/membro/buscaPorUser', { headers: { 'Authorization' : `Bearer ${token}`}}).then(res => {
            const membrosCarga = res.data.sucesso.value;
            AsyncStorage.setItem(StorageKeys.CARGA_MEMBROS, JSON.stringify(membrosCarga));
            navigation.navigate('PageLider')
        })

        await setLoading(true);
        await setModalAtualizacao(!modalAtualizacao);
    }

    useEffect(() => {
        consultarStorage();
    }, [])

    if (!carregamento) {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="black" />
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <PageHeader title="Minhas Informações" corRede={`${corRede}`} />

            <ScrollView>

                <View style={styles.body}>

                    <View style={styles.bodyCard}>

                        <View style={styles.bodyCardHeader}>
                            <Feather name="user" size={24} color={`${corRede}`} />
                            <Text style={styles.bodyCardTitle}>Meu perfil</Text>
                        </View>
                        <View style={styles.bodyCardSession}>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Apelido: </Text>
                                {usuario?.apelido}
                            </Text>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Nome: </Text>
                                {usuario?.nomeCompleto}
                            </Text>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Barracão: </Text>
                                {usuario?.igreja.nome}
                            </Text>
                            <Text style={styles.bodyCardSessionText}>
                                <Text style={styles.bodyCardSessionTitle}>Rede: </Text>
                                {usuario?.rede.cor}
                            </Text>
                            <View style={styles.bodyCardSessionDataNasc}>
                                <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Data nasc.: </Text>
                                    {UtilsHelper.dataAniversario(usuario?.dataNascimento as string)}
                                </Text>
                                {/* <Text style={styles.bodyCardSessionText}>
                                    <Text style={styles.bodyCardSessionTitle}>Idade:</Text>
                                    {UtilsHelper.calcularIdade(usuario?.dataNascimento as string)}
                                </Text> */}
                            </View>
                        </View>

                    </View>

                    <View style={styles.bodyCard}>

                        <View style={styles.bodyCardHeader}>
                            <MaterialCommunityIcons name="dna" size={24} color={`${corRede}`} />
                            <Text style={styles.bodyCardTitle}>Células</Text>
                        </View>

                        {celulas?.map(celula => {
                            return (
                                <View style={styles.bodyCardSession}>
                                    <Text style={styles.bodyCardSessionText}>
                                        <Text style={styles.bodyCardSessionTitle}>Nome: </Text>
                                        {celula.nome}
                                    </Text>

                                    <Text style={styles.bodyCardSessionText}>
                                        <Text style={styles.bodyCardSessionTitle}>Lideres: </Text>
                                        {UtilsHelper.nomesLideres(celula.lideres)}
                                    </Text>

                                    <View style={styles.bodyCardSessionDataNasc}>
                                        <Text style={styles.bodyCardSessionText}>
                                            <Text style={styles.bodyCardSessionTitle}>Horario: </Text>
                                            {celula.horario}
                                        </Text>
                                        <Text style={styles.bodyCardSessionText}>
                                            <Text style={styles.bodyCardSessionTitle}>dia Sem.:</Text>
                                            {celula.dia}
                                        </Text>
                                    </View>

                                </View>
                            )
                        })}

                    </View>

                    <View style={styles.footerViewButtons}>

                        <View style={styles.footer}>
                            <TouchableOpacity style={[styles.footerButton, { backgroundColor: `${corRede}` }]}
                                onPress={() => setModalAtualizacao(!modalAtualizacao)}>
                                <Feather name="download-cloud" size={26} color="#fff" />
                                <Text style={styles.footerButtonText}>Atualizar Dados</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.footer}>
                            <TouchableOpacity style={styles.footerButton} onPress={() => { setModalVisibleSair(true) }}>
                                <AntDesign name="export2" size={26} color="#fff" />
                                <Text style={styles.footerButtonText}>Sair</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>
            </ScrollView>

            <Modal animationType='slide' transparent={true} visible={modalVisibleSair} onRequestClose={() => { Alert.alert("Modal has been closed."); }}>
                <View style={styles.modalContainer}>
                    <View style={styles.modalBody}>
                        <Text style={styles.modalTitle}>LogOff</Text>
                        <View style={styles.modalInfo}>
                            <Text>Tem certeza que deseja sair da conta?</Text>
                        </View>

                        <View style={styles.modalFooter}>
                            <TouchableOpacity style={styles.modalButtonFechar} onPress={() => { setModalVisibleSair(!modalVisibleSair) }}>
                                <AntDesign style={{ marginRight: 10 }} name="dislike2" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    Não
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.modalButtonOk} onPress={() => { setModalVisibleSair(!modalVisibleSair), logoff() }}>
                                <AntDesign style={{ marginRight: 10 }} name="like2" size={18} color="white" />
                                <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                    Sim
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </Modal>

            <Modal animationType='slide' transparent={true} visible={modalAtualizacao} onRequestClose={() => { Alert.alert("Modal has been closed."); }}>
                <View style={styles.modalContainer}>
                    {!loading ?
                        <View style={styles.modalBody}>
                            <Text style={styles.modalTitle}>Atualização</Text>
                            <View style={styles.modalInfo}>
                                <Text>Tem certeza que deseja baixar as atualizações que foram realizadas em outros dispositivos?</Text>
                            </View>

                            <View style={styles.modalFooter}>
                                <TouchableOpacity style={styles.modalButtonFechar} onPress={() => { setModalAtualizacao(!modalAtualizacao) }}>
                                    <AntDesign style={{ marginRight: 10 }} name="dislike2" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        Não
                                    </Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={styles.modalButtonOk} onPress={() => { baixarInformacoes() }}>
                                    <AntDesign style={{ marginRight: 10 }} name="like2" size={18} color="white" />
                                    <Text style={{ color: "white", fontWeight: "bold", textAlign: "center", marginRight: 5 }}>
                                        Sim
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        : <ModalLoading />
                    }


                </View>
            </Modal>
        </SafeAreaView>
    );
}

export default Ajuste;